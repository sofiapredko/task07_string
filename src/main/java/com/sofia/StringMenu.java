package com.sofia;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.out;

import static java.lang.System.out;

public class StringMenu {

    public static void actionWithString() {
        String data = null;
        try {
            data = StringFunctions.readFileAsString("C:\\Users\\Sofia\\Documents\\Books QA\\testfile.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
        out.println(data);

        out.println("Sentences: ");
        String[] arrOfSent = StringFunctions.splitTextBySentence(data);
        StringFunctions.printStringArr(arrOfSent);

        out.println("Words: ");
        String[] arrOfWords = StringFunctions.splitTextByWord(data);
        //StrFunctions.printStringArr(arrOfWords);


        out.println("List of sentences: ");
        List<String> listAllSentence = Arrays.asList(arrOfSent);
        out.println(listAllSentence);

        out.println("List of words: ");
        List<String> listWords = Arrays.asList(arrOfWords);
        //out.println(listWords);

        Scanner scan = new Scanner(System.in);
        out.println("Enter your choice:");
        int choice = scan.nextInt();
        switch (choice) {
            case 1:
                StringFunctions.numOfSentence(listAllSentence);
                break;
            case 2:
                StringFunctions.sortBySize(listAllSentence);
                break;
            case 3:
                StringFunctions.onlyInFirstSentence(arrOfSent);
                break;
            case 5:
                StringFunctions.replaceWithLongest(listAllSentence);
                break;
            case 6:
                out.println(StringFunctions.sortByAlphabet(listWords));
                break;
            case 8:
                StringFunctions.sortVowels(listWords);
                break;
            case 10:
                StringFunctions.sortByFrequency(listWords);
                break;
            case 14:
                out.println(StringFunctions.getPalindromeList(listAllSentence));
                break;
        }
    }
}
