package com.sofia;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static java.lang.System.out;
import static java.util.Collections.sort;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class StringFunctions {
    public static String readFileAsString(String fileName) throws Exception {
        String data = "";
        data = new String(Files.readAllBytes(Paths.get(fileName)));
        return data;
    }

    public static void sortBySize(List<String> list) {
        out.println("-- sorting list of string --");
        sort(list, Comparator.comparing(String::length));
        list.forEach(out::println);
    }

    public static void sortStriArrByLength(String[] stringArray) {
        out.println("-- sorting array of string --");
        Arrays.sort(stringArray, Comparator.comparing(String::length));
        Arrays.stream(stringArray).forEach(out::println);
    }

    public static String[] splitTextBySentence(String data) {
        String[] arrOfS = data.split(" *[?!.] *");
        return arrOfS;
    }

    public static String[] splitTextByWord(String data) {
        String[] arrOfW = data.split(" ");
        return arrOfW;
    }

    public static void printStringArr(String[] arr) {
        for (String a : arr) {
            out.println(a);
        }
    }

    public static boolean isPalindrome(String value) {
        if (value == null || value.isEmpty())
            return false;
        return new StringBuilder(value).reverse().toString().equals(value);
    }

    public static boolean isPalindromeUsingIntStream(String text) {
        String temp = text.replaceAll("\\s+", "").toLowerCase();
        return IntStream
                .range(0, temp.length() / 2)
                .noneMatch(i -> temp.charAt(i) != temp.charAt(temp.length() - i - 1));
    }

    public static List<String> getPalindromeList(List<String> list) {
        List<String> palindromeList = new ArrayList<String>();
        for (String e : list) {
            if (isPalindromeUsingIntStream(e))
                palindromeList.add(e);
        }
        return palindromeList;
    }

    public static void sortByFrequency(List<String> list) {
        out.println("Frequency of each word: ");
        Map<String, Long> mapc =
                list.stream()
                        .map(String::toLowerCase)
                        .collect(groupingBy(Function.identity(), counting()));
        out.println(mapc);

        Map<String, Long> treeMap = new TreeMap<>(mapc);
        for (String str : treeMap.keySet()) {
            System.out.println(str);
        }

        /*Set<String> st = new HashSet<String>(list);
        for (String s : st)
            System.out.println(s + ": " + Collections.frequency(list, s));   */
    }

    public static int numOfSentence(List<String> listAll) {
        int count = 0;
        for (int i = 0; i < listAll.size(); i++) {
            String data = listAll.get(i);
            String[] arrOfW = data.split(" ");
            List<String> listWords = Arrays.asList(arrOfW);

            Set<String> set = new HashSet<>();
            Set<String> duplicateElements = new HashSet<>();

            for (String element : listWords) {
                if (!set.add(element)) {
                    duplicateElements.add(element);
                }
            }
            if(!duplicateElements.isEmpty()) count++;
            System.out.println("Duplicate Elements in " + i + " sentence: " + duplicateElements);
        }
        out.println("Count of such sentences: " + count);
        return count;
    }

    public static List<String> sortByAlphabet(List<String> listW){
        listW.sort(String.CASE_INSENSITIVE_ORDER);
        //Collections.sort(listW, String.CASE_INSENSITIVE_ORDER);
        return listW;
    }

    public static void sortVowels(List<String> listWords){
        String pattern = "\\b[aeiouy]+[a-z]*\\b";
        List<String> vowelList = new ArrayList<>();
        Pattern p = Pattern.compile(pattern);
        for(String e : listWords){
            e.toLowerCase();
            Matcher m = p.matcher(e);
            if(m.matches()){
                vowelList.add(e);
            }
        }

        vowelList.sort(String.CASE_INSENSITIVE_ORDER);
        Set<String> vowelSet = new LinkedHashSet<String>(vowelList);
        out.println(vowelSet);
    }

    public static String findLongest(List<String> listWords){
        return listWords.stream().max(Comparator.comparingInt(String::length)).orElse("n/a");
    }

    public static void replaceWithLongest(List<String> listAllSentence) {
        for (int i = 0; i < listAllSentence.size(); i++) {
            String data = listAllSentence.get(i);
            String[] arrOfW = data.split(" ");
            List<String> listWords = Arrays.asList(arrOfW);

            String pattern = "\\b[aeiouy]+[a-z]*\\b";

            String longest = findLongest(listWords);
            int longestIndex = listWords.indexOf(longest);

            Pattern p = Pattern.compile(pattern);

            for (String e : listWords) {
                Matcher m = p.matcher(e);

                if (m.matches()) {
                    out.println("Longest: " + longest + ", vowel word: " + e);
                    int indexMaxWord = listWords.indexOf(longest);
                    int indexVowel = listWords.indexOf(e);
                    listWords.set(indexMaxWord, e);
                    listWords.set(indexVowel, longest);
                    break;
                }
            }
            out.println("List of replaced in " + i + "th sentence: " + listWords);
        }
    }

    public static void onlyInFirstSentence(String[] arrOfSent) {
        String firstSentence = arrOfSent[0];
        String[] arrOfWordsinFirst = splitTextByWord(firstSentence);

        List<String> listWordsInFirst = new ArrayList(Arrays.asList(arrOfWordsinFirst));
        out.println("first sen words" + listWordsInFirst);

        List modifiableList = new ArrayList(Arrays.asList(arrOfSent));
        modifiableList.remove(0);

        String all = "";

        for(Object e : modifiableList){
            e.toString();
            all += e;
        }
        String[] arrAll = splitTextByWord(all);
        List<String> listWordsAll = Arrays.asList(arrAll);
        listWordsInFirst.removeAll(listWordsAll);
        out.println("Words that exist only in first sentebce: " + listWordsInFirst);
    }



}
