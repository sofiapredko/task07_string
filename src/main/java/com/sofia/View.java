package com.sofia;

import java.util.*;

public class View {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    Locale locale;
    ResourceBundle bundle;

    private void setMenu() {

        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("Q", bundle.getString("Q"));
    }

    public View() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", StringMenu::actionWithString);
        methodsMenu.put("2", this::internationalizeMenuUkrainian);
        methodsMenu.put("3", this::internationalizeMenuEnglish);
        methodsMenu.put("4", this::internationalizeMenuFrench);
        methodsMenu.put("5", this::internationalizeMenuTurkish);
        methodsMenu.put("6", this::internationalizeMenuJapanese);

    }


    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuFrench() {
        locale = new Locale("fr");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }
    private void internationalizeMenuTurkish() {
        locale = new Locale("tr");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuJapanese() {
        locale = new Locale("ja");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                System.out.println(menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
